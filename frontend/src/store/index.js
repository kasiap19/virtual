import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isAuth: false,
    notification: {
      display: false,
      msg: null,
      status: null
    }
  },
  mutations: {
    IS_AUTH(state, paylaod) {
      state.isAuth = paylaod
    },
    DISPLAY_NOTIFICATION(state, payload) {
      state.notification = payload
    },
    HIDE_NOTIFICATION(status) {
      status.notification = {
        display: false,
        msg: null,
        status: null
      }
    }
  },
  actions: {
  },
  modules: {
    user
  }
})

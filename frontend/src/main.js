import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'


import VueApollo from 'vue-apollo';

Vue.config.productionTip = false
Vue.use(VueApollo)


const cache = new InMemoryCache()
const httpLink = createHttpLink({
  uri: '/graphql/', 
})

const apolloClient = new ApolloClient({
  link: httpLink,
  cache, 
  connectToDevTools: true
})


// Install the vue plugin
// With the apollo client instance
const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})

new Vue({
  router,
  store,
  apolloProvider, 
  render: h => h(App)
}).$mount('#app')

import gql from 'graphql-tag'

//////////////////////////////// MUTATIONS //////////////////////////////////

// create new user
export const NEW_USER = gql`
mutation ( $name: String, $surname: String, $email: String, $password: String){
    newUser( name:$name, surname: $surname, email: $email, password: $password) {
        user {
            id, 
            name
        }
    }
}`;

// login
export const LOGIN = gql`
mutation ($email: String, $password: String){
    login(email: $email, password: $password) {
        user {
            id,
            name,
        }
    } 
}`;

// log out
export const LOG_OUT = gql`
mutation($id: String)  {
    logout(id: $id) {
        msg
    } 
}`;

// edit user
export const EDIT_USER = gql`
mutation($id: String, $name: String, $surname: String, $email: String, $phone: String, $cpf: String, $street: String, $house: String, $postcode: String)  {
    editUser(id: $id, name: $name, surname: $surname, email: $email, phone: $phone, cpf: $cpf, street: $street, house: $house, postcode: $postcode) {
        user {
            id 
            name 
            surname 
            email 
            phone 
            cpf 
            street 
            house 
            postcode 
        }
    } 
}`;

// change password
export const CHANGE_PASSWORD = gql`
mutation($id: String, $oldPassword: String, $newPassword: String)  {
    changePassword(id: $id, oldPassword: $oldPassword, newPassword: $newPassword) {
        msg
    } 
}`;


//////////////////////////////// QUERIES //////////////////////////////////

// verify user
export const VERIFY_USER = gql`
query {
    verifyUser {
        id 
        name 
        surname 
        email 
        phone  
        cpf 
        street 
        house 
        postcode 
    } 
}`;
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '@/store/index'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/shopping-list',
    name: 'shoppingList',
    component: () => import(/* webpackChunkName: "shopping-list" */ '../views/ShoppingList.vue')
  },
  {
    path: '/checkout',
    name: 'checkout',
    component: () => import(/* webpackChunkName: "checkout" */ '../views/Checkout.vue')
  },
  { path: '/user/:id', 
    beforeEnter:(to, from, next) => {
      // console.log(store.state.isAuth)
      next()
    },
  
  name: 'user',
  component: () => import(/* webpackChunkName: "user" */ '../views/User.vue')},
]

// const isAuthenticated = 
const router = new VueRouter({
  mode: 'history',
  // base: process.env.BASE_URL,
  routes
})

export default router

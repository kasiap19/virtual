import uuid
from django.db import models
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractBaseUser, Permission, PermissionsMixin
from django.utils.translation import gettext as _

class UserManager(BaseUserManager):
    """
    extending base user manager to create users and superusers with the custom user model
    """

    def create_user(self, email, password=None, **extra_fields):
        """create user"""
        if not email:
            raise ValueError('Email must be provided')
        user = self.model(
            email=self.normalize_email(email).lower(),
            **extra_fields,
        )
        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        """create superuser"""
        user = self.create_user(email, password, **extra_fields)
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self.db)
        return user

# Create your models here.
class User(AbstractBaseUser, PermissionsMixin):
    """User model"""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    email = models.EmailField(_('email address'), unique=True)
    password = models.CharField(max_length=100)
    phone = models.CharField(max_length=100) 
    cpf = models.CharField(max_length=11)
    street = models.CharField(max_length=100)
    house = models.CharField(max_length=100)
    postcode = models.CharField(max_length=100) 
    
    objects = UserManager()

    USERNAME_FIELD = 'email' 
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = []

    @property
    def is_staff(self):
        """required for compatibility with Django's built in admin site"""
        return self.is_superuser


    def __str__(self): 
        return self.email

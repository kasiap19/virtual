import graphene
from graphene_django.types import DjangoObjectType, ObjectType
from backend.models import User
from pecas.types.types import  UserType
from django.contrib.sessions.models import Session

class Query(ObjectType):
    # user view 
    verify_user = graphene.Field(UserType)
    
    def resolve_verify_user(self, info, **kwargs):   
        cookis = info.context.COOKIES
    
        if 'sessionid' in cookis:
            session = Session.objects.get(session_key=cookis["sessionid"])
            uid = session.get_decoded().get('_auth_user_id')

            return User.objects.get(pk=uid)
 
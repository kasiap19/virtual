from django.contrib import admin
from django.urls import path, re_path
from backend.views import vue_app
from graphene_django.views import GraphQLView
from django.views.decorators.csrf import csrf_exempt
from graphql_jwt.decorators import jwt_cookie
from .schema import schema

urlpatterns = [
    path('admin/', admin.site.urls),
    path('graphql/' , jwt_cookie(csrf_exempt(GraphQLView.as_view(schema=schema, graphiql=True)))),

    path('', vue_app, name="index"),
    re_path(r'^.*', vue_app),
]
 
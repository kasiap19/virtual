# schema which combines all abstract queries (schemas from apps)
from graphene import ObjectType, Schema
from .queries.userQuery import Query as UserQuery 
from .mutations.user import Mutation as User 
import graphql_jwt
 
class Query(UserQuery):
    pass 
 

class Mutation(User):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    pass


schema = Schema(query=Query, mutation=Mutation)

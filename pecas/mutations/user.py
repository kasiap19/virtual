import graphene
from pecas.types.types import UserType
from backend.models.user import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import update_session_auth_hash


"""
Create new user
"""
class NewUser(graphene.Mutation):
    class Arguments: 
        name = graphene.String()
        surname = graphene.String()
        email = graphene.String()
        password = graphene.String()

    user = graphene.Field(UserType) 
    msg = graphene.String() 

    def mutate(self, info, email, password, name, surname):
        find_user = User.objects.filter(email=email).exists()
        if find_user:
            return NewUser(msg="User already exists.")
        else: 
            user = User.objects.create_user(email=email, password=password, name=name, surname=surname)
            return NewUser(user=user) 

"""
Edit user
"""
class EditUser(graphene.Mutation):
    class Arguments: 
        id = graphene.String()
        name = graphene.String()
        surname = graphene.String()
        email = graphene.String()
        phone = graphene.String()
        cpf = graphene.String()
        street = graphene.String()
        house = graphene.String()
        postcode = graphene.String()

    user = graphene.Field(UserType)
    def mutate(self, info, id, name, surname, email, phone, cpf, street, house, postcode):
        user = User.objects.get(id=id)

        if user:
            user.name = name
            user.surname = surname
            user.emai = email
            user.phone = phone
            user.cpf = cpf
            user.street = street
            user.house = house
            user.house = house
            user.postcode = postcode

            user.save()
            return EditUser(user=user)

"""
Change Password
"""
class ChangePassword(graphene.Mutation):
    class Arguments:
        id = graphene.String()
        old_password = graphene.String()
        new_password = graphene.String()

    msg = graphene.String()
    def mutate(self, info, id, old_password, new_password):
        user = User.objects.get(id=id)

        if user and user.check_password(old_password):
            user.set_password(new_password)
            user.save()
            update_session_auth_hash(info.context, user)

        
        return ChangePassword(msg='xxx')

"""
Login
"""
class Login(graphene.Mutation):
    class Arguments: 
        email = graphene.String()
        password = graphene.String()
    
    user = graphene.Field(UserType)
    msg = graphene.String() 
    

    def mutate(self, info, email, password):
        user = authenticate(email=email, password=password)

        if not user:
            return Login(msg="Email or password is incorrect.") 

        if user.is_authenticated:  
            login(info.context, user)
            info.context.session.set_expiry(3600) 

            return Login(user=user) 

"""
Log out
"""
class Logout(graphene.Mutation):
    class Arguments: 
        id = graphene.String()

    msg = graphene.String() 

    def mutate(self, info, id):
        user = User.objects.get(id=id) 
        if user: 
            logout(info.context)
            # info.context.COOKIES -> clear csrftoken ?
            return Logout(msg="Logged Out")  


class Mutation(graphene.ObjectType):
    new_user = NewUser.Field() 
    login = Login.Field()
    logout = Logout.Field() 
    edit_user = EditUser.Field()
    change_password = ChangePassword.Field()
